CREATE TABLE tbl_category (
    id int primary key auto_increment ,
    category_name varchar (20),
);

CREATE TABLE tbl_article (
    id int primary key auto_increment,
    title varchar (100),
    description varchar (200),
    author varchar (100),
    thumbnail varchar (100),
    create_date varchar (50),
    category_id int,
    FOREIGN  KEY (category_id) REFERENCES tbl_category (id) on DELETE  CASCADE
);
INSERT INTO tbl_category (category_name) VALUES ('java');
INSERT INTO tbl_category (category_name) VALUES ('spring');
INSERT INTO tbl_category (category_name) VALUES ('c#');
INSERT INTO tbl_category (category_name) VALUES ('other');

INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('A to Z', 'Something that is great', 'john', 'snow.png', '12/2/2018',1);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('Java is basic', 'Something that is great', 'john', 'snow.png', '12/2/2018',1);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('Java learning', 'Something that is great', 'john', 'snow.png', '12/2/2018',1);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('A to Z', 'Something that is great', 'john', 'snow.png', '12/2/2018',1);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('C#', 'Something that is great', 'john', 'snow.png', '12/2/2018',3);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('JSON', 'Something that is great', 'john', 'snow.png', '12/2/2018',4);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('A to Z', 'Something that is great', 'john', 'snow.png', '12/2/2018',1);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('spring course', 'Something that is great', 'john', 'snow.png', '12/2/2018',2);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('Learn programming in one hour', 'Something that is great', 'john', 'snow.png', '12/2/2018',4);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('spring course', 'Something that is great', 'john', 'snow.png', '12/2/2018',2);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('Java is basic', 'Something that is great', 'john', 'snow.png', '12/2/2018',1);
INSERT INTO tbl_article (title, description, author, thumbnail, create_date , category_id) VALUES ('Learn programming in one hour', 'Something that is great', 'john', 'snow.png', '12/2/2018',4);