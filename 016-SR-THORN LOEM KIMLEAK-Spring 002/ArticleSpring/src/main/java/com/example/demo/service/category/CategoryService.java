package com.example.demo.service.category;

import com.example.demo.model.Article;
import com.example.demo.model.ArticleFilter;
import com.example.demo.model.Category;
import com.example.demo.utility.Paging;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CategoryService {

    List<Category> findAll();
    Category findById(int id);
    void editCate(Category category);

    List<Article> findAllFilter(@Param("filter") ArticleFilter filter, @Param("paging") Paging paging);

    void addCategory(Category category);

}
