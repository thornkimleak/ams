package com.example.demo.Configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class FileUploadConfiguration implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        String filedir = "file:"+System.getProperty("user.dir")+"\\src\\main\\resources\\images\\";
        registry.addResourceHandler("/Image/**").addResourceLocations(filedir);
    }
}
