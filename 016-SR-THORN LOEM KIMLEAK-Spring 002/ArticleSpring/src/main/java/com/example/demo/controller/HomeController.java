package com.example.demo.controller;

import com.example.demo.model.Article;
import com.example.demo.model.ArticleFilter;
import com.example.demo.model.Category;
import com.example.demo.service.ArticleService;
import com.example.demo.service.category.CategoryService;
import com.example.demo.utility.Paging;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
public class HomeController {

    ArticleService articleService ;
    private CategoryService categoryService;
    @Autowired
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    @Autowired
    public void setCategoryService(CategoryService categoryService){this.categoryService=categoryService;}
    @GetMapping("/")
    public String index(@Param("filter") ArticleFilter filter, @Param("paging") Paging paging, Model m){
        //
        //String title,Integer id,
        List<Article> articles = articleService.findAllFilter(filter,paging);
        List<Category> categories = categoryService.findAll();
        System.out.println("*****");
        m.addAttribute("categories",categories);
       // m.addAttribute("articles",articleService.findAll());
        m.addAttribute("articles",articles);
        m.addAttribute("filter",filter);
        m.addAttribute("paging", paging);
        System.out.println(categories.size());
        articles.forEach(System.out::println);
       // System.out.println("title:"+title+"id"+id);
        return "index";
    }

    @GetMapping("/add")
    public String add(ModelMap m,@ModelAttribute Article article){
        m.addAttribute("article",new Article());
        m.addAttribute("categories", categoryService.findAll());
        System.out.println("GetMapping : Add");
        return "add";
    }
    @PostMapping("/add")
    public String insert(@Valid @ModelAttribute Article article, BindingResult result,Model m, @RequestParam("file") MultipartFile file){
        System.out.println("PostMapping : Add");
        System.out.println(article.toString());
        if(result.hasErrors()){
            m.addAttribute("article",article);
            m.addAttribute("category",article.getCategory().getId());
            return "add";
        }else {
            String filename = UUID.randomUUID().toString() + file.getOriginalFilename();
            if(!file.isEmpty()){
                System.out.println(file.getOriginalFilename());
                try {
                    Files.copy(file.getInputStream(), Paths.get(System.getProperty("user.dir")+"\\src\\main\\resources\\images\\", filename));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            article.setImageName(filename);
            System.out.println(article.getImageName());

            article.setCategory(categoryService.findById(article.getCategory().getId()));
            articleService.add(article);
            return "redirect:/";
        }

    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Integer id){
        articleService.delete(id);
        return "redirect:/";
    }
    @GetMapping("/update/{id}")
    public String update(Model m, @PathVariable Integer id){
        Article article = articleService.findById(id);
        System.out.println(article.getCategory());
        m.addAttribute("article", article);
        m.addAttribute("categories",categoryService.findAll());
        System.out.println("GetMapping : Update");
        return "update";
    }
    @PostMapping("/update")
    public String saveUpdate(@Valid @ModelAttribute Article article,BindingResult result,Model m,@RequestParam("file") MultipartFile file){
        System.out.println("PostMapping : Update");
        System.out.println("ID of category" + article.getCategory().getId());
        System.out.println(article);
        if(result.hasErrors()){
            m.addAttribute("article",article);
            m.addAttribute("category",article.getCategory());
            m.addAttribute("isAdd", false);
            return "/add";
        }else {
            article.setCreatedDate(new Date().toString());
            String filename = UUID.randomUUID().toString() + file.getOriginalFilename();
            if (!file.isEmpty()) {
                System.out.println(file.getOriginalFilename());
                try {
                    Files.copy(file.getInputStream(), Paths.get(System.getProperty("user.dir") + "\\src\\main\\resources\\images\\", filename));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            article.setImageName(filename);
            System.out.println(article);
            articleService.update(article);
            article.setCategory(categoryService.findById(article.getCategory().getId()));
            return "redirect:/";
        }
    }
    @GetMapping("/view/{id}")
    public String viewItem(Model m,@PathVariable Integer id){
        Article article = articleService.findById(id);
        m.addAttribute("article",article);
        return "view";
    }


}
