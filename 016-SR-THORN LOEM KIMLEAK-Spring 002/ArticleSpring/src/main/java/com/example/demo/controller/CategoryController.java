package com.example.demo.controller;

import com.example.demo.model.Article;
import com.example.demo.model.Category;
import com.example.demo.repository.ArticleRepository;
import com.example.demo.service.ArticleService;
import com.example.demo.service.category.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class CategoryController {
    @Autowired
    ArticleService articleService ;
    @Autowired
    private CategoryService categoryService;
    @GetMapping("/addCategory")
    public String addCategory(ModelMap m, @ModelAttribute Category category){
        System.out.println("GetMapping category: Get");
        m.addAttribute("categories",new Category());
        return "addCategory";
    }
    @PostMapping("/addCategory")
    public String insertCategory(Model m, @ModelAttribute Category category){
        System.out.println("PostMapping category : Add");
        categoryService.addCategory(category);
        return "redirect:addCategory";
    }

    @GetMapping("/listCategory")
    public String listCategory(ModelMap m,@ModelAttribute Category category){
        List<Category> categories=categoryService.findAll();
        categories.forEach(System.out::println);
        m.addAttribute("categories",categories);
        return "listCategory";
    }
    @GetMapping("/editCategory/{id}")
    public String editCate(Model m, @PathVariable Integer id){
        System.out.println("editCate");
        Category category = categoryService.findById(id);
        m.addAttribute("category",category);
        return "editCategory";

    }

    @PostMapping("/editCategory")
    public String saveEditCate(@ModelAttribute Category category, BindingResult result, Model m){
//        articleService.update(article);
//        article.setCategory(categoryService.findById(article.getCategory().getId()));
        categoryService.editCate(category);
        return "redirect/";
    }
}
