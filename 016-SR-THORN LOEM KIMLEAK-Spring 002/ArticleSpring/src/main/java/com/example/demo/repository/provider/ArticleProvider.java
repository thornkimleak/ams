package com.example.demo.repository.provider;

import com.example.demo.model.Article;
import com.example.demo.model.ArticleFilter;
import com.example.demo.model.Category;
import com.example.demo.utility.Paging;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.stereotype.Repository;

@Repository
public class ArticleProvider {
    public String findAll(Integer id){
        return new SQL(){{
            SELECT("*");
            FROM("TBL_ARTICLE");
            if(id!=null){
                WHERE("id ="+id);
            }
            ORDER_BY("id");

        }}.toString();
    }
    public String update(Article article){
        return new SQL(){{
            UPDATE("tbl_article");
            SET("title = '"+article.getTitle()+"'");
            SET("description = '"+article.getDescription()+"'");
            SET("author ='"+article.getAuthor()+"'");
            SET("create_date = '"+article.getCreatedDate()+"'");
            SET("thumbnail='"+article.getImageName()+"'");
            SET("category_id="+article.getCategory().getId());
            WHERE("id = '"+article.getId()+"'");
        }}.toString();
    }
    public String editCategory(Category category){
        return new SQL(){{
           UPDATE("tbl_category");
           SET("name = '"+category.getName()+"'");
           WHERE("id = '"+category.getId()+"'");

        }}.toString();
    }

    public String countAllArticle(Article article){
        return  new SQL(){{
            SELECT("COUNT(id)");
            FROM("tbl_article");


        }}.toString();
    }

    public String findAllFilter(@Param("filter") ArticleFilter filter, @Param("paging") Paging paging){
        return new SQL(){{
            SELECT("*");
            FROM("tbl_article ");
            if(filter.getTitle() !=null){
                WHERE("title ILIKE '%' ||  #{filter.title} || '%' ");
            }
            if(filter.getCate_id() !=null){
                WHERE("category_id=#{filter.cate_id}");
            }
            ORDER_BY("id ASC LIMIT #{paging.limit} OFFSET #{paging.offset}");

        }}.toString();

    }
    public String findFilterOne(){
        return new SQL(){{
            SELECT("*");
            FROM("tbl_category");
            WHERE("id=#{id}");
        }}.toString();
    }
    public String countAllArticles(ArticleFilter filter){
        return  new SQL(){{
            SELECT("COUNT(id)");
            FROM("tbl_article");
            if(filter.getTitle() !=null){
                WHERE("title ILIKE '%' ||  #{title} || '%' ");
            }
            if(filter.getCate_id() !=null){
                WHERE("category_id=#{cate_id}");
            }

        }}.toString();
    }
    public String findOne(){
        return new SQL(){{
            SELECT("*");
            FROM("tbl_category");
            WHERE("id=#{id}");
        }}.toString();
    }

}
