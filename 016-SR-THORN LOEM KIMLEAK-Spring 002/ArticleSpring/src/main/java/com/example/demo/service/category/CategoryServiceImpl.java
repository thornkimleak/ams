package com.example.demo.service.category;

import com.example.demo.model.Article;
import com.example.demo.model.ArticleFilter;
import com.example.demo.model.Category;
import com.example.demo.repository.ArticleRepository;
import com.example.demo.repository.CategoryRepository;
import com.example.demo.utility.Paging;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ArticleRepository articleRepository;


    public void setCategoryRepository(CategoryRepository categoryRepository){
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category findById(int id) {
        return categoryRepository.findById(id);
    }

    @Override
    public void editCate(Category category) {

    }

    @Override
    public List<Article> findAllFilter(@Param("filter") ArticleFilter filter, @Param("paging") Paging paging) {
        return articleRepository.findAllFilter(filter,paging);
    }

    @Override
    public void addCategory(Category category) {
        categoryRepository.addCategory(category);
    }
}
