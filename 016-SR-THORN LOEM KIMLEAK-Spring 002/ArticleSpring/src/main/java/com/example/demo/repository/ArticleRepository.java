package com.example.demo.repository;

import com.example.demo.model.Article;
import com.example.demo.model.ArticleFilter;
import com.example.demo.model.Category;
import com.example.demo.repository.provider.ArticleProvider;
import com.example.demo.utility.Paging;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ArticleRepository {
    @Select("SELECT * FROM tbl_article")
    @Results({
            @Result(property ="createdDate",column = "create_date"),
            @Result(property ="imageName",column = "thumbnail"),
            @Result(property = "category", column = "category_id", one = @One(select = "getCategoryOfArticle")),
    })
    List<Article> findAll();


    @Select("SELECT * FROM tbl_category WHERE id = #{cateId}")
    @Results({
            @Result(property = "name", column = "category_name"),

    })
    Category getCategoryOfArticle(int cateId);


    @SelectProvider(method = "findAll",type = ArticleProvider.class)
    @Results({
            @Result(property ="createdDate",column = "create_date"),
            @Result(property ="imageName",column = "thumbnail"),
            @Result(property = "category", column = "category_id", one = @One(select = "getCategoryOfArticle"))

    })
    Article findById(int id);


    @Insert("insert into TBL_ARTICLE(title,description,author,create_date,thumbnail,category_id) values"+
    "(#{title},#{description},#{author},#{createdDate},#{imageName},#{category.id})")
    @Results({
            @Result(property = "createdDate",column = "create_date"),
            @Result(property = "imageName",column = "thumbnail")
    })
    void add(Article article);


    @Select("delete from TBL_ARTICLE where id=#{id}")
    void delete(int id);


    @SelectProvider(method = "update",type = ArticleProvider.class)
    void update(Article article);



    @SelectProvider(method ="findAllFilter",type = ArticleProvider.class)
    @Results({
            @Result(property="createdDate",column = "create_date"),
            @Result(property = "imageName",column = "thumbnail"),
            @Result(property="category",column = "category_id", one = @One(select ="findFilterOne"))
    })
    List<Article> findAllFilter(@Param("filter") ArticleFilter filter, @Param("paging") Paging paging);


    @SelectProvider(method = "findOne",type = ArticleProvider.class)
    @Results({
            @Result(property="name",column = "category_name"),
    })
    Category findFilterOne(int id);


    @SelectProvider(method = "countAllArticles",type = ArticleProvider.class)
    public Integer countAllArticles(ArticleFilter filter);


}
