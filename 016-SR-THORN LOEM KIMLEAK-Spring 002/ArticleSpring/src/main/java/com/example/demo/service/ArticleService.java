package com.example.demo.service;

import com.example.demo.model.Article;
import com.example.demo.model.ArticleFilter;
import com.example.demo.utility.Paging;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface ArticleService {
    List<Article> findAll();
    Article findById(int id);
    void add(Article article);
    void delete(int id);
    void  update(Article article);


    List<Article> findAllFilter(@Param("filter") ArticleFilter filter, @Param("paging") Paging paging);
}
