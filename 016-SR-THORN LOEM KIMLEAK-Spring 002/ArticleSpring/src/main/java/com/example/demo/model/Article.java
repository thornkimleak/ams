package com.example.demo.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class Article {

   
    private Integer id;

    @NotBlank(message = "* Field cannot be blank")
    private String title;

    @NotBlank(message = "* Field cannot be blank")
    private String description;

    @NotBlank(message = "* Field cannot be blank")
    @Size(min = 3, max = 20,message = "* length must be > 3 and <20")
   public String author;
   public String createdDate;
    public String imageName;
    public Category category;

    public Article(Integer id, @NotBlank(message = "* Field cannot be blank") String title, @NotBlank(message = "* Field cannot be blank") String description, @NotBlank(message = "* Field cannot be blank") @Size(min = 3, max = 20, message = "* length must be > 3 and <20") String author, String createdDate, String imageName, Category category) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.author = author;
        this.createdDate = createdDate;
        this.imageName = imageName;
        this.category = category;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Article() {
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", author='" + author + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", imageName='" + imageName + '\'' +
                ", category=" + category +
                '}';
    }
}
