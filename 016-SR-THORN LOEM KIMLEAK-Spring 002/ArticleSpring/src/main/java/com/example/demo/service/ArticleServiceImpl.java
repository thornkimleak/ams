package com.example.demo.service;

import com.example.demo.model.Article;
import com.example.demo.model.ArticleFilter;
import com.example.demo.repository.ArticleRepository;
import com.example.demo.utility.Paging;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService {
    @Autowired
    private ArticleRepository articleRepository;

    public void setArticleRepository(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }
    @Override
    public List<Article> findAll() {
        return articleRepository.findAll();
    }

    @Override
    public Article findById(int id)
    {
        return articleRepository.findById(id);
    }

    public void add(Article article){
        articleRepository.add(article);
    }
    public void delete(int id){
        articleRepository.delete(id);
    }
    public  void update(Article article){
        article.setCreatedDate(new Date().toString());
        articleRepository.update(article) ;
    }

    public List<Article> findAllFilter(@Param("filter") ArticleFilter filter, @Param("paging") Paging paging){
        paging.setTotalCout(articleRepository.countAllArticles(filter));
        return  articleRepository.findAllFilter(filter,paging);
    }


//    public int getLastIndex(){
//        return articleRepository.getLastIndex();
//    }
}
