package com.example.demo.repository;

import com.example.demo.model.Category;
import com.example.demo.repository.provider.ArticleProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {

    @Select("select  * from tbl_category")
    @Results({
            @Result(property = "name",column = "category_name"),
            @Result(property = "id",column = "id")
    })
    List<Category> findAll();

    @Select("SELECT * FROM TBL_CATEGORY WHERE ID = #{id}")
    @Results({
            @Result(property = "name",column = "category_name"),
            @Result(property = "id",column = "id")
    })
    Category findById(int id);


    @Insert("insert into tbl_category (id,category_name) values (#{id},#{name})")
    @Results({
            @Result(property = "id",column = "id"),
            @Result(property = "name",column = "category_name")

    })
    void addCategory(Category category);

    @SelectProvider(method = "editCategory",type = ArticleProvider.class)
    void editCategory (Category category);


}
